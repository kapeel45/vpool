package com.vpool.carpool;

import org.springframework.data.repository.CrudRepository;

import com.vpool.model.DailyCarpool;

public interface DailyCarPoolDao extends CrudRepository<DailyCarpool, Long>{
	
	
}
