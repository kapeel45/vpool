package com.vpool.carpool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vpool.model.DailyCarpool;

@Service
public class DailyCarpoolService {

	@Autowired
	DailyCarPoolDao dailyCarPoolDao;
	
	public DailyCarpool getCarPool(String id){
		return dailyCarPoolDao.findOne(Long.parseLong(id));
	}
	
}
