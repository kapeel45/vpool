package com.vpool.carpool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vpool.model.DailyCarpool;

@RestController
public class CarPoolController{

	@Autowired
	DailyCarpoolService dailyCarpoolService;
	
	@RequestMapping("/run")
	public String sayHi(){
		return "Running...";
	}
	
	@RequestMapping("/dailycarpool/{id}")
	public DailyCarpool getTopic(@PathVariable String id){
		return dailyCarpoolService.getCarPool(id);
	}
	
}
